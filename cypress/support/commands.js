// ***********************************************
// created custom commands and overwrite
// existing commands for request method.
// ***********************************************

Cypress.Commands.add('requestGet', { log : false }, (path, respCode) => {
    cy.request({
        method:'GET', 
        url:path, 
        failOnStatusCode: false
    }).then((response) => {
        expect(response.status).to.eq(respCode)
        expect(response.body).is.not.null
        return response
    })
})
