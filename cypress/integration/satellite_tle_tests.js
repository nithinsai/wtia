// *************************************************************
//Feature: To test the service - "satellites/[id]/tles" 
//This file contains both positive and negative scenarios
//test-data : No pre-requisites to set up any test data
// *************************************************************

///<reference types="Cypress" />

const dataJson = require('../fixtures/tleresponse')

describe('WTIA : Validate API satellites/[id]/tles', () => {
    
     it('GET TLES for an invalid ISS NORAD ID - Status 404', () => {           
        cy.requestGet('/satellites/0/tles', 404).then( (response) => {
            expect(response.body).to.have.property('status', 404)
            expect(response.body).to.have.property('error', 'satellite not found')
        })        
    })

    it('GET TLES for an invalid ISS NORAD ID and suppress_response_codes=true - Status 200 with error', () => {     
        cy.requestGet('/satellites/0/tles?suppress_response_codes=true', 200).then( (response) => {
            expect(response.body).to.have.property('status', 404)
            expect(response.body).to.have.property('error', 'satellite not found')
        })
    })

    it('GET the satellite id TLES for a valid ISS NORAD ID - JSON response', () => {  
        //Getting a valid id from the satellites path. If the ISS id is constant, I would skip this step to get the id(i.e., see below test)
        cy.requestGet('/satellites/', 200).then((response) => {
            const issId = response.body[0].id
            return issId
        }).then((issId) => {
            cy.request('/satellites/' + issId + '/tles').its('headers').its('content-type').should('include', 'application/json')
            cy.requestGet('/satellites/' + issId + '/tles', 200).then( (response) => {
                expect(response.body).to.deep.include(dataJson)
                expect(response.body).to.have.property('requested_timestamp').is.not.null; 
                expect(response.body).to.have.property('line1').is.not.null; 
                expect(response.body).to.have.property('line2').is.not.null; 
            })
        })
    })

    it('GET the satellite id TLES for a valid ISS NORAD ID - Text response', () => {  
        //Assuming the valid issID = 25544 as If I am setting up data, I would use a constant id to load the database 
            let path = '/satellites/' + dataJson.id + '/tles?format=text';
            cy.request('/satellites/25544/tles?format=text').its('headers').its('content-type').should('include', 'text/plain')
            cy.requestGet('/satellites/25544/tles?format=text', 200).then( (response) => {
                expect(response.body).to.not.have.property('requested_timestamp'); 
                expect(response.body).to.include('ISS') 
            })
    })

})