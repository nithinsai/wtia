// *************************************************************
//Feature: To test the service - "satellites/[id]/positions" 
//This file contains both positive and negative scenarios
//test-data : No pre-requisites to set up any test data
// *************************************************************

///<reference types="Cypress" />

const dataJson = require('../fixtures/positions')
const path = '/satellites/25544/positions?timestamps='

describe('WTIA : Validate API satellites/[id]/positions', () => {
    
     it('GET positions for an empty time stamp - Status 400', () => {           
        cy.requestGet(path, 400).then( (response) => {
            expect(response.body).to.have.property('status', 400)
            expect(response.body).to.have.property('error', 'invalid timestamp in list: ')
        })        
      })

      it('GET positions for an invalid satellite id - Status 404', () => {           
        cy.requestGet('/satellites/255454/positions?timestamps=', 404).then( (response) => {
            expect(response.body).to.have.property('status', 404)
            expect(response.body).to.have.property('error', 'satellite not found')
        })   
      })      

      it('GET positions for iss satellite id for one timestamp', () => {    
        cy.requestGet(path + dataJson[0].timestamp + '&units=miles', 200).then( (response) => {
            expect(response.body).to.be.an('array');
            expect(response.body).to.have.length(1);
            expect(response.body).to.deep.include(dataJson[0])
        })        
      })
      
      it('GET positions for iss satellite id for multiple timestamps', () => {    
        let timestamps=""
        for(let i=0; i<dataJson.length; i++){
          timestamps += dataJson[i].timestamp + ","
        }
        timestamps = timestamps.substring(0, timestamps.length-1)
        cy.requestGet( path + timestamps + '&units=miles', 200).then( (response) => {
            expect(response.body).to.be.an('array');
            expect(response.body).to.have.length(dataJson.length);
            expect(response.body).to.deep.equals(dataJson)
        })        
      }) 
      
      it('GET positions for iss satellite id for one timestamp - kilometers', () => {    
        cy.requestGet(path + dataJson[0].timestamp + '&units=kilometers', 200).then( (response) => {
            expect(response.body).to.be.an('array');
            expect(response.body).to.have.length(1);
            expect(response.body[0]).to.have.property('units','kilometers')
        })        
      })
  })