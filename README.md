The tests are written using Cypress.

Pre-requisties: 
Node.js 14 and above

How to Run:
To Install Cypress via npm/cmd terminal:
    cd /local/project/path
    npm install cypress --save-dev
Assuming, no erros in the above steps and you are at /local/project/path in npm/cmd:
    /local/project/path>.\node_modules\.bin\cypress run
It will display the status on cmd window(attached a refernce .\project\path\backup-results\cmdResult.JPG) 

If you want an html report, then proceed with below steps: 
(However, I have attached a report in the project path at '.\project\path\backup-results\mocha-report.html')
    npm install mocha
    npm install mochawesome --save-dev
    npm install mochawesome-merge --save-dev
Run Cypress tests again with a reporter tag this time
    npx cypress run --reporter mochawesome
    npx mochawesome-merge cypress/report/mochawesome-report/*.json > cypress/report/output.json
    npx marge cypress/report/output.json --reportDir ./ --inline


Observations:
* satellites/{id}/positions
        - path could have been better if it is satellite/{id}/positions as we are giving a specific id
        - There could have been better validation for timestamp(it doesn't have a format)
        - The limit of timestamps says 10, but it accepts more than limit i.e., 10
        - Until 10 requests, it validates value after ','(comma delimited list) but after it reaches 10 values, it processes the empty  values too. And the timestamp returns as 0

* satellites/{id}/tles
        - path could have been better if it is satellite/{id}/tles as we are giving a specific id       
        - Response pattern could have been similar for both JSON and Text formats for mutliple requests 